module ConcurrentActions where

import Control.Concurrent
import qualified Data.ByteString.Lazy as L
import qualified Codec.Compression.Zlib as Z
import qualified Codec.Compression.GZip as G

data Async a = Async (MVar a)

async :: IO a -> IO (Async a)
async action = do
  var <- newEmptyMVar
  forkIO (action >>= putMVar var)
  return (Async var)

wait :: Async a -> IO a
wait (Async var) = readMVar var

data Queue = Queue Bool [Action]
data Action = Compression CompressionType String (Async ())

data CompressionType =
    Zip
  | Gzip
  deriving (Eq, Show)

compression :: CompressionType -> L.ByteString -> L.ByteString
compression Zip  = Z.compress
compression Gzip = G.compress

compress :: CompressionType -> FilePath -> FilePath -> IO ()
compress ctype from to = do
  content <- L.readFile from
  L.writeFile to $ (compression ctype) content
