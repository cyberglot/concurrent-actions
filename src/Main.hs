module Main where

import ConcurrentActions
import Control.Monad (forever)
import System.Exit (exitSuccess)

main :: IO ()
main = do
  let queue = Queue False []
  putStrLn "welcome !"
  putStrLn options
  run queue

options :: String
options = "compress your files \n\
          \try `zip filename` or `gzip filename` \n\
          \or `quit` to finish \n\
          \`queue` to check out your processes \n\
          \`help` to see this again"

showActions :: [Action] -> String
showActions [] = ""
showActions (x:xs) =
  case x of
    (Compression ctype name _) ->
      "compressing " ++ name ++ " as " ++ show ctype
      ++ "\n" ++ showActions xs
    _                          -> showActions xs

awaitAll :: [Action] -> IO [()]
awaitAll actions = mapM (\(Compression _ _ var) -> wait var) actions

run :: Queue -> IO ()
run (Queue True [])  = do
  putStrLn "bye!"
  exitSuccess
run (Queue True actions)  = do
  putStrLn "waiting all actions to finish..."
  awaitAll actions
  putStrLn "all actions have been processed. bye!"
  exitSuccess
run queue = forever $ do
  putStr "> "
  option <- getLine
  handleAction queue option >>= run

handleAction :: Queue -> String -> IO (Queue)
handleAction (Queue isQuit actions) option = do
  let (cmd:etc) = words option

  case cmd of
    ("quit") -> do
      return (Queue True actions)
    ("zip")  -> do
      let (filename:_) = etc
      putStrLn $ filename ++ " has been added to the queue."
      comp <- async $ compress Zip filename (filename ++ ".zip")
      return (Queue False (actions ++ [Compression Zip filename comp]))
    ("gzip") -> do
      let (filename:_) = etc
      putStrLn $ filename ++ " has been added to the queue."
      comp <- async $ compress Gzip filename (filename ++ ".gzip")
      return (Queue False (actions ++ [Compression Gzip filename comp]))
    ("queue") -> do
      putStrLn $ showActions actions
      return (Queue isQuit actions)
    ("help") -> do
      putStrLn options
      return (Queue isQuit actions)
    _        -> do
      putStrLn "that ain't a valid command. try again."
      return (Queue isQuit actions)
